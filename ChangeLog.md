# Changelog for contracheck-applicative

## 0.2.0 Major update
   * Add `CheckPatch`-module and functionality
   * Add SOP-support
   * Deprecate Control.Validation.Class
   * Update documentation

## 0.1.3 Update documentation

## 0.1.1.1 Update documentation

## 0.1.1.0 Drop Checkable instances for Text and ByteString
